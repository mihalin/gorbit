from statistics import mean
import sys
from PyQt5.QtWidgets import QApplication
from PyQt5.QtCore import QTimer
from argparse import ArgumentParser
from constants import *
from genetic import Genetic, copy_black_hole
from world import World, random_black_holes
from plotter import Plotter
from control import ControlPanel
import saver


def make_save_name(reason: str, generation: int, name: str = "") -> str:
    return "_".join(filter(None, (name, reason, str(generation),)))


if __name__ == "__main__":

    parser = ArgumentParser(description="Поиск устойчивых орбит с помощью генетических алгоритмов")
    parser.add_argument("--run", dest="run", type=str, help="Запустить сохранённый мир из файла (без обучения)")
    parser.add_argument("--holes", dest="holes", type=str, help="Взять систему чёрных дыр из файла")
    parser.add_argument("--limit", dest="limit", type=int, help="Отобразить только лучшие N планет")
    parser.add_argument("--show_banned", dest="banned", action="store_true", default=False,
                        help="Отобразить только выброшеные планеты")
    parser.add_argument("--draw_mode", dest="draw_mode", action="store_true", default=False,
                        help="Запуск в режиме рисования карты")
    parser.add_argument("--name", dest="name", type=str, help="Указать имя эксперимента", default="")
    parser.add_argument("--use_graph", dest="graph",  help="Использовать графику", action="store_true")
    parser.add_argument("--no_graph", dest="graph", help="Не использовать графику", action="store_false")
    parser.set_defaults(graph=True)
    args = parser.parse_args()

    app = QApplication(sys.argv)

    genetic = Genetic()
    world = World(app, draw_mode=args.draw_mode, name=args.name, use_graph=args.graph)
    plotter = Plotter(name=args.name)
    control = ControlPanel(name=args.name)
    control.setGraphMode.connect(lambda enable: world.set_use_graph(enable))

    black_holes = []
    result = {}

    if args.draw_mode:
        control.savePopulation.connect(lambda name: saver.save(name, world.drawed_holes, {}))
    else:
        control.savePopulation.connect(lambda name: saver.save(name, black_holes, result))

    control.pause.connect(world.set_pause)

    def run_world():
        global black_holes, result
        black_holes, result = saver.read(args.run)
        limit = args.limit if args.limit else len(result)
        generation = sorted(result, key=lambda x: result[x], reverse=True)[:limit]
        if args.banned:
            generation = [x for x in result.keys() if result[x] < 0]
        result = world.draw_generation(generation, black_holes, timeout, f"поколение {args.run}")
        print(f"Best: {result}")

    def loop():
        global black_holes, result
        generation = genetic.next_generation({})

        if args.holes:
            black_holes, _ = saver.read(args.holes)
        else:
            black_holes = random_black_holes(8, screen_total_size, 0.7)

        best = 0
        while True:
            black_holes = [copy_black_hole(hole) for hole in black_holes]
            result = world.test_generation(generation, black_holes, timeout, f"поколение {genetic.generation_number}")
            generation = genetic.next_generation(result)

            # статистика
            new_best = max(result.values())
            avg = mean(result.values())

            # сохранить успех (если опция включена)
            if new_best > best and control.save_every_progress:
                saver.save(make_save_name("progress", int(new_best), args.name), black_holes, result)
                print("Прогресс сохранён")

            best = new_best

            # сохранить бан (если опция включена)
            worst = min(result.values())
            if worst < 0 and control.save_every_banned:
                saver.save(make_save_name("banned", genetic.generation_number, args.name), black_holes, result)
                print("Бан сохранён")

            plotter.add_next_data(best, avg)

    if args.run:
        QTimer.singleShot(0, run_world)
    elif args.draw_mode:
        pass
    else:
        QTimer.singleShot(0, loop)

    sys.exit(app.exec_())
