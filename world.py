import pygame
import pymunk
from typing import Tuple, List, Dict, Union
from enum import IntEnum
from enum import auto as enum_auto
from PyQt5.QtWidgets import QWidget
from PyQt5.QtGui import QImage, QPainter, QMouseEvent
from PyQt5.QtCore import QTimer
import PyQt5.QtCore as QtCore
from PyQt5.QtCore import pyqtSignal
from random import randint
from time import time

from utils import random_color, make_title
from constants import *
from faster import faster


class ImageWidget(QWidget):
    clicked = pyqtSignal(int, int)
    cleared = pyqtSignal()

    def __init__(self, surface, parent=None):
        super(ImageWidget, self).__init__(parent)
        self._surface = surface

        self.resize(*screen_visible_size.int_tuple)
        self.move(0, 0)

    def paintEvent(self, event):
        qp = QPainter()
        w = self._surface.get_width()
        h = self._surface.get_height()
        data = self._surface.get_buffer().raw
        image = QImage(data, w, h, QImage.Format_RGB32)
        qp.begin(self)
        qp.drawImage(0, 0, image)
        qp.end()

    def keyPressEvent(self, event):
        if event.key() == QtCore.Qt.Key_Escape:
            exit(0)
        if event.key() == QtCore.Qt.Key_C:
            self._surface.fill(Colors.black)
            self.cleared.emit()

    def mousePressEvent(self, event: QMouseEvent):
        if event.button() != QtCore.Qt.RightButton:
            return
        self.clicked.emit(event.x(), event.y())


def gravity_force(bh: pymunk.Body, body: pymunk.Body) -> Vec2d:
    return faster.force(body.position, bh.position, body.mass, bh.meta_body.mass)


def scaled(x: Union[List[int], int]) -> Union[List[int], int]:
    if isinstance(x, int):
        return scaled([x])[0]
    return [int(v / screen_scale) for v in x]


def unscaled(x: Union[List[int], int]) ->Union[List[int], int]:
    if isinstance(x, int):
        return unscaled([x])[0]
    return [int(v * screen_scale) for v in x]


class AstroBodyType(IntEnum):
    BLACK_HOLE = enum_auto()
    PLANET = enum_auto()
    OTHER = enum_auto()


class AstroBody:

    def __init__(self, position: Tuple[int, int], mass: int, velocity: int = 0, angle: float = 0,
                 body_type: AstroBodyType = AstroBodyType.OTHER, color: Tuple[int, int, int] = None):
        from math import sin, cos, pi

        self._start_position = Vec2d(position)
        self._mean_position = Vec2d(-100, -100)
        self._mean_position_diff = self._mean_position
        self._mean_counter = 0
        self._cycled = False
        self._mass = mass
        self._start_velocity = velocity
        angle_rad = angle * 2 * pi / 360
        self._velocity = Vec2d(velocity * cos(angle_rad), velocity * sin(angle_rad))
        self._start_angle = angle

        bodyt = pymunk.Body.DYNAMIC
        if body_type == AstroBodyType.BLACK_HOLE:
            bodyt = pymunk.Body.STATIC
        
        self._body = pymunk.Body(mass, 1, body_type=bodyt)
        self._body.position = position
        #self._body.velocity = Vec2d(velocity * cos(angle_rad), velocity * sin(angle_rad))
        self._shape = pymunk.Circle(self._body, planet_radius)

        self._body.meta_body = self
        self._shape.meta_body = self

        self._type = AstroBodyType(body_type)

        self._color = Colors.white

        if color is not None:
            self._color = color
        elif self._type == AstroBodyType.BLACK_HOLE:
            self._color = Colors.red
        elif self._type == AstroBodyType.PLANET:
            self._color = random_color()
        else:
            self._color = Colors.white

    @property
    def color(self) -> Tuple[int, int, int]:
        return self._color

    @property
    def position(self):
        return self._body.position

    @position.setter
    def position(self, value):
        self._body.position = value

    def in_rect(self, rect: Tuple[Vec2d, Vec2d]) -> bool:
        pos = self.position
        if rect[0].x < pos.x < rect[1].x and rect[0].y < pos.y < rect[1].y:
            return True
        return False

    @property
    def start_position(self) -> Vec2d:
        return self._start_position

    @property
    def mass(self) -> int:
        return self._mass

    @property
    def start_velocity(self) -> int:
        return self._start_velocity

    @property
    def velocity(self):
        return self._velocity

    @velocity.setter
    def velocity(self, value):
        self._velocity = value

    @property
    def start_angle(self) -> float:
        return self._start_angle

    @property
    def shape(self) -> pymunk.Shape:
        return self._shape

    @property
    def body(self) -> pymunk.Body:
        return self._body

    @property
    def body_type(self) -> AstroBodyType:
        return self._type

    def __repr__(self):
        return f"AstroBody {self.position} {self.velocity} {self.mass}"


def random_black_holes(count: int, screen_size: Tuple[int, int], center: float = 1) -> List[AstroBody]:
    result = []

    assert center <= 1
    w, h = screen_size
    wmax = int(w * center)
    hmax = int(h * center)

    xmin = (w - wmax) // 2
    xmax = xmin + wmax
    ymin = (h - hmax) // 2
    ymax = ymin + hmax
    for _ in range(count):
        x = randint(xmin, xmax)
        y = randint(ymin, ymax)
        m = randint(hole_min_mass, hole_max_mass)
        result.append(AstroBody((x, y), m, body_type=AstroBodyType.BLACK_HOLE))

    return result


def random_planets(count: int, screen_size: Tuple[int, int]) -> List[AstroBody]:
    result = []
    for _ in range(count):
        x = randint(0, screen_size[0])
        y = randint(0, screen_size[1])
        m = randint(planet_min_mass, planet_max_mass)
        speed = randint(0, planet_max_speed)
        angle = randint(0, 360)

        result.append(AstroBody((x, y), m, speed, angle, body_type=AstroBodyType.PLANET))

    return result


class World:
    def __init__(self, app, draw_mode: bool = False, name: str = "", use_graph: bool = True):
        pygame.init()
        self._screen = pygame.Surface(screen_visible_size)
        widget = ImageWidget(self._screen)
        self._window = widget
        self._window.show()

        self._app = app
        self._name = name

        self._use_graph = use_graph
        self._window.setVisible(use_graph)
        self._pause = False

        self._last_test_time = time()

        self._drawed_black_holes = []
        if draw_mode:
            widget.clicked.connect(self.drawed_black_hole)
            widget.cleared.connect(self.cleared_black_holes)

    def drawed_black_hole(self, x: int, y: int):
        m = randint(hole_min_mass, hole_max_mass)
        self._drawed_black_holes.append(AstroBody(unscaled([x, y]), m, body_type=AstroBodyType.BLACK_HOLE))
        for hole in self._drawed_black_holes:
            pygame.draw.circle(self._screen, hole.color, scaled([int(x) for x in hole.body.position]),
                               scaled(hole_radius))

        self._window.update()

    def set_pause(self, pause: bool):
        self._pause = pause

    def cleared_black_holes(self):
        self._drawed_black_holes.clear()
        self._window.update()

    @property
    def drawed_holes(self):
        return self._drawed_black_holes

    def set_use_graph(self, use: bool):
        self._use_graph = use
        self._window.setVisible(use)

    def draw_generation(self, planets: List[AstroBody], black_holes: List[AstroBody], time_limit: int,
                        caption: str):
        screen = self._screen
        self._window.setWindowTitle(make_title(self._name, caption))

        lifetimes = {}

        space = pymunk.Space()
        space.gravity = (00.0, 00.0)

        time_spend = 0

        # Добавление чёрных дыр
        for hole in black_holes:
            space.add(hole.body, hole.shape)

        # Добавление планет
        for planet in planets:
            space.add(planet.body, planet.shape)

        def remove_planet(planet: AstroBody, spec_time: int = None):
            planets.remove(planet)
            space.remove(planet.shape, planet.body)

        screen_total_rect = (Vec2d(0, 0), screen_total_size)

        screen.fill(Colors.black)

        self._app.processEvents()

        time_spend = 0

        while True:

            self._app.processEvents()

            if self._pause:
                continue

            if not planets:
                break

            # физика
            for planet in planets[:]:
                planet.position = faster.next_position(planet, black_holes)
                if not planet.in_rect(screen_total_rect):
                    remove_planet(planet)
                    continue

            time_spend += 1

            for planet in planets:
                pos = scaled([int(x) for x in planet.body.position])
                rad = scaled(planet_radius)
                pygame.draw.circle(screen, planet.color, pos, rad)

            for hole in black_holes:
                pygame.draw.circle(screen, hole.color, scaled([int(x) for x in hole.body.position]),
                                   scaled(hole_radius))

            self._window.update()

        return time_spend

    def test_generation(self, planets: List[AstroBody], black_holes: List[AstroBody], time_limit: int,
                        caption: str) -> Dict[AstroBody, float]:
        screen = self._screen
        self._window.setWindowTitle(make_title(self._name, caption))

        self._app.processEvents()

        lifetimes = faster.test_generation(planets, black_holes)

        return dict(zip(planets, lifetimes))


if __name__ == "__main__":

    from PyQt5.QtWidgets import QApplication
    import sys

    black_holes = random_black_holes(3, screen_total_size, 0.7)
    planets = random_planets(100, screen_total_size)
    caption = "Случайное поколение"

    app = QApplication(sys.argv)

    world = World(app)

    QTimer.singleShot(0, lambda: world.test_generation(planets, black_holes, timeout, caption))

    sys.exit(app.exec_())
