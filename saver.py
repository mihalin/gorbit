from world import AstroBody, AstroBodyType
from json import dump, load
from typing import List, Dict, Tuple


def body_to_dict(body: AstroBody, fitness: float = -1) -> Dict:
    d = {
        "mass": body.mass,
        "position": (body.start_position.x, body.start_position.y),
        "velocity": body.start_velocity,
        "angle": body.start_angle,
        "body_type": body.body_type.value,
        "color": body.color,
        "fitness": fitness
    }
    return d


def dict_to_body(d: Dict) -> AstroBody:
    dd = d.copy()
    dd.pop("fitness")
    b = AstroBody(**dd)
    return b


def save(name: str, black_holes: List[AstroBody], planets: Dict[AstroBody, float]):
    with open(name, "w") as file:
        data = {
            "black_holes": [body_to_dict(b) for b in black_holes],
            "planets": [body_to_dict(b, planets[b]) for b in planets.keys()]
        }
        dump(data, file, indent=2)


def read(name: str) -> Tuple[List, Dict]:
    with open(name, "r") as file:
        json = load(file)
        black_holes = [dict_to_body(d) for d in json["black_holes"]]
        planets = {dict_to_body(d): d["fitness"] for d in json["planets"]}

        return black_holes, planets


if __name__ == "__main__":
    from world import random_black_holes, random_planets
    from constants import screen_total_size

    planets_write = {planet: 0 for planet in random_planets(10, screen_total_size)}
    holes_write = random_black_holes(10, screen_total_size, 0.8)

    save("test", holes_write, planets_write)

    holes_read, planets_read = read("test")
