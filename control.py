from PyQt5.QtWidgets import QInputDialog, QVBoxLayout, QWidget, QPushButton, QCheckBox
from PyQt5.QtCore import pyqtSignal
from utils import make_title


class ControlPanel(QWidget):
    setGraphMode = pyqtSignal(bool)
    savePopulation = pyqtSignal(str)
    pause = pyqtSignal(bool)
    _mode = True
    _pause = False

    def __init__(self, name: str = ""):
        super().__init__()
        layout = QVBoxLayout()
        self.setLayout(layout)

        self.setWindowTitle(make_title(name, "Управление"))

        graph_btn = QPushButton("Графика")
        layout.addWidget(graph_btn)
        graph_btn.clicked.connect(self.mode_clicked)

        pause_btn = QPushButton("Пауза")
        layout.addWidget(pause_btn)
        pause_btn.clicked.connect(self.pause_clicked)

        save_population_btn = QPushButton("Сохранить последнюю")
        layout.addWidget(save_population_btn)
        save_population_btn.clicked.connect(self.save_population_clicked)

        exit_btn = QPushButton("Выход")
        layout.addWidget(exit_btn)
        exit_btn.clicked.connect(lambda: exit(0))

        self._save_every_banned = QCheckBox("Сохранять забаненые")
        layout.addWidget(self._save_every_banned)

        self._save_every_progress = QCheckBox("Сохранять новые этапы эволюции")
        layout.addWidget(self._save_every_progress)

        self.move(900, 600)
        self.show()

    def pause_clicked(self):
        self._pause = not self._pause
        self.pause.emit(self._pause)

    def mode_clicked(self):
        self._mode = not self._mode
        self.setGraphMode.emit(self._mode)

    def save_population_clicked(self):
        text, ok = QInputDialog.getText(self, 'Сохранить популяцию', 'Введите имя сохранения:')
        if ok:
            self.savePopulation.emit(str(text))

    @property
    def save_every_banned(self):
        return self._save_every_banned.isChecked()

    @property
    def save_every_progress(self):
        return self._save_every_progress.isChecked()


if __name__ == "__main__":
    from PyQt5.QtWidgets import QApplication
    import sys

    app = QApplication(sys.argv)

    panel = ControlPanel()
    panel.show()

    sys.exit(app.exec_())
