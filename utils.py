from typing import Tuple


def random_color() -> Tuple[int, int, int]:
    from random import randint
    return randint(50, 128), randint(50, 128), randint(50, 128)


def make_title(name: str, subname: str) -> str:
    return " - ".join(filter(None, (name, subname)))
