from pymunk import Vec2d

screen_visible_size = Vec2d(800, 600)
planet_radius = 5
hole_radius = 15
hole_max_mass = 3E6
hole_min_mass = 3E6
planet_max_mass = 200
planet_min_mass = 20
planet_max_speed = 5
timeout = 10000
screen_scale = 2.5
screen_total_size = screen_visible_size * screen_scale
population_size = 100
use_ban = False


class Colors:
    white = (255, 255, 255)
    black = (0, 0, 0)
    red = (255, 0, 0)
