from PyQt5.QtWidgets import QMainWindow, QSizePolicy
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.figure import Figure
from utils import make_title


class Plotter(QMainWindow):

    def __init__(self, name: str = ""):
        super().__init__()
        self.left = 10
        self.top = 10
        self.title = make_title(name, 'Статистика обучения')
        self.width = 640
        self.height = 400

        self._plot = None
        self.initUI()

        self._best = []
        self._mean = []

    def initUI(self):
        self.setWindowTitle(self.title)
        self.setGeometry(self.left, self.top, self.width, self.height)

        self._plot = PlotCanvas(self, width=5, height=4)
        self._plot.move(0, 0)

        self.move(900, 0)

        self.show()

    def add_next_data(self, best: float, mean: float) -> None:
        self._best.append(best)
        self._mean.append(mean)

        self._plot.plot(self._mean, self._best)


class PlotCanvas(FigureCanvas):

    def __init__(self, parent=None, width=5, height=4, dpi=100):
        fig = Figure(figsize=(width, height), dpi=dpi)

        FigureCanvas.__init__(self, fig)
        self.setParent(parent)

        FigureCanvas.setSizePolicy(self,
                QSizePolicy.Expanding,
                QSizePolicy.Expanding)
        FigureCanvas.updateGeometry(self)

    def plot(self, mean, best):
        self.figure.clear()
        ax_mean = self.figure.add_subplot(211)
        ax_mean.plot(mean, 'r-')
        ax_mean.set_title('Mean')
        ax_best = self.figure.add_subplot(212)
        ax_best.plot(best, 'r-')
        ax_best.set_title('Best')
        self.draw()


if __name__ == '__main__':
    from PyQt5.QtWidgets import QApplication
    import sys

    app = QApplication(sys.argv)
    ex = Plotter()
    ex.add_next_data(1, 2)
    ex.add_next_data(3, 4)
    ex.add_next_data(3, 4)
    sys.exit(app.exec_())
