from random import random, choice
from deap import base
from deap import creator
from deap import tools
from typing import List
from itertools import chain
from numpy import clip
from copy import deepcopy

from typing import Dict

from constants import *
from world import AstroBody, AstroBodyType, random_black_holes, World
from plotter import Plotter


def evaluate(individual):
    # Do some hard computing on the individual
    a = sum(individual)
    return a,


def gen_to_body(gen: List[float], is_best: bool = False) -> AstroBody:
    body = AstroBody((gen[0] * screen_total_size.x, gen[1] * screen_total_size.y),
                     planet_min_mass + gen[2] * (planet_max_mass - planet_min_mass),
                     gen[3] * planet_max_speed,
                     gen[4] * 360, AstroBodyType.PLANET,
                     color=Colors.red if is_best else None)
    body.gen = gen
    return body


def copy_black_hole(hole: AstroBody) -> AstroBody:
    return AstroBody(hole.start_position, hole.mass, body_type=AstroBodyType.BLACK_HOLE)


class Genetic:
    def __init__(self):
        genes = ["x", "y", "mass", "velocity", "angle"]

        # знак веса - минимизация или максимизация
        creator.create("FitnessMax", base.Fitness, weights=(1.0,))
        creator.create("Individual", list, fitness=creator.FitnessMax)
        toolbox = base.Toolbox()

        toolbox.register("individual", tools.initRepeat, creator.Individual,
                         random, n=len(genes))

        # Создать случайную популяцию
        self._population = [toolbox.individual() for _ in range(population_size)]

        self._generation_number = 0

    @property
    def generation_number(self):
        return self._generation_number

    def _tournament(self, population: List) -> List:
        def tournament():
            # selTournament([c1, c2...], 1, 2) выбирает случайных двух индивидов и из них выбирает одного лучшего
            return deepcopy(tools.selTournament(population, 1, population_size // 2))
        selected = deepcopy(tools.selBest(population, 2))
        selected += list(chain(*[tournament() for _ in range(population_size - 2)]))
        return selected

    def _roulette(self, population: List) -> List:
        def roulette():
            return deepcopy(tools.selRoulette(population, 1))
        selected = list(chain(*[roulette() for _ in range(population_size)]))
        return selected

    def _best(self, population: List) -> List:
        c = population_size//2  # Доля отбираемых особей
        best = tools.selBest(population, population_size//c)
        selected = list(chain(*[deepcopy(best) for _ in range(c)]))
        return selected

    def next_generation(self, prev_result: Dict[AstroBody, float]) -> List[AstroBody]:
        self._generation_number += 1

        population = self._population
        planets = [gen_to_body(gen) for gen in population]

        if not prev_result:
            return planets

        for body in prev_result.keys():
            body.gen.fitness.values = (prev_result[body], )

        # Взять одну лучшую особь (потом оставить её без изменения в популяции)
        best, = deepcopy(tools.selBest(population, 1))

        # Выбрать лучших
        population = self._roulette(population)

        # Горизонтальный обмен генами
        # for _ in range(population_size // 3):
        #   parent1, parent2 = choice(population), choice(population)
        #   tools.cxBlend(parent1, parent2, 0.5)

        # Мутации случайных
        for i in range(len(population)):
            # mu - центр распределения, sigma - ширина Гауса, indpb - вероятность изменения гена
            tools.mutGaussian(population[i], mu=0.0, sigma=1E-10, indpb=0.9)
            pass

        # Мутации случайных (более жёсткие и менее вероятные)
        for i in range(len(population)):
            # mu - центр распределения, sigma - ширина Гауса, indpb - вероятность изменения гена
            tools.mutGaussian(population[i], mu=0.0, sigma=0.06, indpb=0.3)
            pass

        # Проверка валидности генов
        for i in range(len(population)):
            for x in range(len(population[i])):
                population[i][x] = clip(population[i][x], 0, 1)

        population.pop()
        population.append(best)

        self._population = population

        return [gen_to_body(gen, is_best=(gen == best)) for gen in population]


if __name__ == "__main__":
    import sys
    from PyQt5.QtWidgets import QApplication
    from PyQt5.QtCore import QTimer

    app = QApplication(sys.argv)

    genetic = Genetic()
    world = World(app)
    plotter = Plotter()

    def loop():
        generation = genetic.next_generation({})
        black_holes = random_black_holes(10, screen_total_size, 0.8)

        while True:
            black_holes = [copy_black_hole(hole) for hole in black_holes]
            result = world.test_generation(generation, black_holes, timeout, f"Generation_{genetic.generation_number}")
            generation = genetic.next_generation(result)

            # статистика
            best = max(result.values())
            mean = sum(result.values())/len(result.values())

            plotter.add_next_data(best, mean)

    QTimer.singleShot(0, loop)

    sys.exit(app.exec_())
