from os.path import dirname, abspath, join as join_path
import ctypes
from ctypes import Structure, CDLL, c_float, byref, c_uint64
from pymunk import Vec2d
from typing import List
from constants import screen_total_size, timeout


lib_path = join_path(abspath(dirname(__file__)), "libfaster.so")

lib = CDLL(lib_path)


class StaticBody(Structure):
    _fields_ = [
        ("x", c_float),
        ("y", c_float),
        ("m", c_float)
    ]


class DynamicBody(Structure):
    _fields_ = [
        ("x", c_float),
        ("y", c_float),
        ("m", c_float),
        ("vx", c_float),
        ("vy", c_float),
    ]


class World(Structure):
    _fields_ = [
        ("max_x", c_float),
        ("max_y", c_float),
        ("min_x", c_float),
        ("min_x", c_float),
        ("max_time", c_uint64),
    ]


# void next(dynamic_body_t *planet, static_body_t *holes, const int holes_count);
lib.next.argtypes = [ctypes.POINTER(DynamicBody), ctypes.POINTER(StaticBody), ctypes.c_int]
lib.next.restype = None

# unsigned long int all_cycles(dynamic_body_t planet, static_body_t *holes, const int holes_count, world_t world);
lib.all_cycles.argtypes = [DynamicBody, ctypes.POINTER(StaticBody), ctypes.c_int, World]
lib.all_cycles.restype = c_uint64


# void test_generation(dynamic_body_t *planets, const int planets_count, static_body_t *holes, const int holes_count,
#                     world_t world, unsigned long int *result);
lib.test_generation.argtypes = [ctypes.POINTER(DynamicBody), ctypes.c_int, ctypes.POINTER(StaticBody),
                                ctypes.c_int, World, ctypes.POINTER(c_uint64)]
lib.test_generation.restype = None


def next_position(planet, holes: List):
    c_planet = DynamicBody(x=planet.position.x, y=planet.position.y,
                           m=planet.mass, vx=planet.velocity.x, vy=planet.velocity.y)
    c_holes = [StaticBody(x=hole.position.x, y=hole.position.y, m=hole.mass) for hole in holes]
    c_holes_arr = (StaticBody * len(holes))(*c_holes)

    lib.next(byref(c_planet), c_holes_arr, ctypes.c_int(len(holes)))
    return Vec2d(c_planet.x, c_planet.y)


def all_cycles(planet, holes: List):
    c_planet = DynamicBody(x=planet.position.x, y=planet.position.y,
                           m=planet.mass, vx=planet.velocity.x, vy=planet.velocity.y)
    c_holes = [StaticBody(x=hole.position.x, y=hole.position.y, m=hole.mass) for hole in holes]
    c_holes_arr = (StaticBody * len(holes))(*c_holes)

    c_world = World(max_x=screen_total_size.x, max_y=screen_total_size.y, min_x=0, min_y=0, max_time=timeout)

    return lib.all_cycles(c_planet, c_holes_arr, ctypes.c_int(len(holes)), c_world)


def test_generation(planets: List, holes: List):
    c_planets = [DynamicBody(x=planet.position.x, y=planet.position.y,
                             m=planet.mass, vx=planet.velocity.x, vy=planet.velocity.y) for planet in planets]
    c_planets_arr = (DynamicBody * len(planets))(*c_planets)
    c_holes = [StaticBody(x=hole.position.x, y=hole.position.y, m=hole.mass) for hole in holes]
    c_holes_arr = (StaticBody * len(holes))(*c_holes)

    c_world = World(max_x=screen_total_size.x, max_y=screen_total_size.y, min_x=0, min_y=0, max_time=timeout)

    result = (c_uint64 * len(planets))()

    lib.test_generation(c_planets_arr, ctypes.c_int(len(planets)), c_holes_arr, ctypes.c_int(len(holes)), c_world,
                        result)

    return list(map(int, result))


if __name__ == '__main__':
    pass
