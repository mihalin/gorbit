#include "faster.h"

#include <math.h>
#include <stdint.h>

static inline float pow2(float value)
{
    return value * value;
}

static inline float pow3(float value)
{
    return value * value * value;
}

static const float float_MIN = 10000;

static inline void _force(dynamic_body_t planet, static_body_t hole, float *force_x, float *force_y)
{
    float r2 = pow2(hole.x - planet.x) + pow2(hole.y - planet.y);
    float r3 = pow3(sqrtf(r2));

    if (r3 < float_MIN)
    {
        *force_x = INFINITY;
        *force_y = INFINITY;
        return;
    }

    float module = hole.m * planet.m / r3;
    *force_x = (hole.x - planet.x) * module;
    *force_y = (hole.y - planet.y) * module;
}

static inline void _force_planet(dynamic_body_t planet, static_body_t *holes, const int holes_count,
                                 float *force_x, float *force_y)
{
    float sum_fx = 0;
    float sum_fy = 0;
    for(int i = 0; i < holes_count; i++)
    {
        float fx;
        float fy;
        _force(planet, holes[i], &fx, &fy);
        sum_fx += fx;
        sum_fy += fy;
    }
    *force_x = sum_fx;
    *force_y = sum_fy;
}

const float TIME = 0.01f;

static inline void _apply_force(dynamic_body_t *planet, float force_x, float force_y)
{
    planet->vx += force_x / planet->m * TIME;
    planet->vy += force_y / planet->m * TIME;
}

static inline void _next_position(dynamic_body_t *planet)
{
    planet->x += planet->vx;
    planet->y += planet->vy;
}

void force(dynamic_body_t planet, static_body_t hole, float *force_x, float *force_y)
{
    return _force(planet, hole, force_x, force_y);
}

static inline void _next(dynamic_body_t *planet, static_body_t *holes, const int holes_count)
{
    float fx, fy;
    _force_planet(*planet, holes, holes_count, &fx, &fy);
    _apply_force(planet, fx, fy);
    _next_position(planet);
}

void next(dynamic_body_t *planet, static_body_t *holes, const int holes_count)
{
    return _next(planet, holes, holes_count);
}

static inline uint64_t _all_cycles(dynamic_body_t planet, static_body_t *holes, const int holes_count, world_t world)
{
    uint64_t t = 0;
    while (t++ < world.max_time)
    {
        _next(&planet, holes, holes_count);
        if (planet.x < world.min_x || planet.x > world.max_x)
            break;
        if (planet.y < world.min_y || planet.y > world.max_y)
            break;
    }
    return t;

}

uint64_t all_cycles(dynamic_body_t planet, static_body_t *holes, const int holes_count, world_t world)
{
    return _all_cycles(planet, holes, holes_count, world);
}

void test_generation(dynamic_body_t *planets, const int planets_count, static_body_t *holes, const int holes_count,
                     world_t world, unsigned long int *result)
{
    for (int i = 0; i < planets_count; i++)
    {
        result[i] = _all_cycles(planets[i], holes, holes_count, world);
    }
}