#ifndef FASTER_H
#define FASTER_H

extern "C"
{
    typedef struct
    {
        float x;
        float y;
        float m;
    } static_body_t;

    typedef struct
    {
        float x;
        float y;
        float m;
        float vx;
        float vy;
    } dynamic_body_t;

    typedef struct
    {
        float max_x;
        float max_y;
        float min_x;
        float min_y;
        unsigned long int max_time;
    } world_t;

    void next(dynamic_body_t *planet, static_body_t *holes, const int holes_count);
    unsigned long int all_cycles(dynamic_body_t planet, static_body_t *holes, const int holes_count, world_t world);
    void test_generation(dynamic_body_t *planets, const int planets_count, static_body_t *holes, const int holes_count,
                         world_t world, unsigned long int *result);
}

#endif // FASTER_H